const Cancel = () => {
    
    return (
        
        <h1 className="font-bold text-white text-center text-3xl mt-20"> Votre paiement a échoué, vous allez être redirigé(e) </h1>
 
    )
}

export default Cancel;