const Success = () => {

    return (
        <h1 className="font-bold text-white text-center text-3xl mt-20">Merci pour votre paiement, à très vite dans notre e-boutique !</h1>
    )
}

export default Success;
