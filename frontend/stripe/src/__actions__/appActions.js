export const SET_DISABLED = "SET_DISABLED";
export const RECORD_ERROR = "RECORD_ERROR";
export const ON_CHANGE = "ON_CHANGE";

//? Données Client et Informations achat --------------------------------------------------------------------------------- 
export const RECORD_PAYMENT_INTENT_ID = "RECORD_PAYMENT_INTENT_ID";
export const RECORD_CUSTOMER_SECRET = "RECORD_CUSTOMER_SECRET";
export const RECORD_CUSTOMER_DATA = "RECORD_CUSTOMER_DATA";

export const RECORD_SUBSCRIPTION_SELECTED = "RECORD_SUBSCRIPTION_SELECTED"; // Subscription Selected 
export const RECORD_EURO_PER_SEAT = "RECORD_EURO_PER_SEAT"; // Subscription Selected 
export const RECORD_PRODUCT_QUANTITY = "RECORD_PRODUCT_QUANTITY"; // Subscription Selected 
export const RECORD_TOTAL_AMOUNT_TO_PAY = "RECORD_TOTAL_AMOUNT_TO_PAY";

export const RECORD_CUSTOMER_PURCHASE_DATA = "RECORD_CUSTOMER_PURCHASE_DATA"; // Subscription Selected 

//? Compteurs et abonnement sélectionné --------------------------------------------------------------------------------- 
export const INCREMENT_PREMIUM_COUNTER = "INCREMENT_PREMIUM_COUNTER";
export const DECREMENT_PREMIUM_COUNTER = "DECREMENT_PREMIUM_COUNTER";

export const INCREMENT_SILVER_COUNTER = "INCREMENT_SILVER_COUNTER";
export const DECREMENT_SILVER_COUNTER = "DECREMENT_SILVER_COUNTER";

//? Objets Prices et Products de l'API --------------------------------------------------------------------------------- 
export const RECORD_PRODUCTS = "RECORD_PRODUCTS";
export const RECORD_PRICES = "RECORD_PRICES";
export const RECORD_PRODUCTS_DATA = "RECORD_PRODUCTS_DATA";
export const RECORD_PRICES_DATA = "RECORD_PRICES_DATA";

//? Paiement finale - données client --------------------------------------------------------------------------------- 
export const RECORD_PAYMENT_METHOD_ID = "RECORD_PAYMENT_METHOD_ID";
export const RECORD_PAYMENT_PROCESSING = "RECORD_PAYMENT_PROCESSING";
export const RECORD_PAYMENT_SUCCESS = "RECORD_PAIMENT_SUCCESS";


